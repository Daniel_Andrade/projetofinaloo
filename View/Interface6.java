package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;

public class Interface6 extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JButton btnVoltar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface6 frame = new Interface6();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Interface6() {
		setTitle("Projeto Final OO");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Buscar Doador");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblNewLabel.setBounds(236, 0, 122, 50);
		contentPane.add(lblNewLabel);
		
		JLabel lblDigiteONome = new JLabel("Digite o nome do doador:");
		lblDigiteONome.setBounds(66, 75, 121, 14);
		contentPane.add(lblDigiteONome);
		
		textField = new JTextField();
		textField.setBounds(197, 72, 161, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(203, 180, 155, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		btnVoltar = new JButton("Voltar");
		btnVoltar.setBounds(453, 120, 89, 23);
		contentPane.add(btnVoltar);
	}

}
