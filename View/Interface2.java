package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JButton;

public class Interface2 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface2 frame = new Interface2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Interface2() {
		setTitle("Projeto Final OO");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDoador = new JLabel("Doador");
		lblDoador.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblDoador.setBounds(227, 0, 65, 37);
		contentPane.add(lblDoador);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.setBounds(32, 81, 89, 23);
		contentPane.add(btnCadastrar);
		
		JButton btnRemover = new JButton("Remover");
		btnRemover.setBounds(32, 115, 89, 23);
		contentPane.add(btnRemover);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.setBounds(32, 149, 89, 23);
		contentPane.add(btnBuscar);
		
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.setBounds(32, 183, 89, 23);
		contentPane.add(btnVoltar);
	}
}
