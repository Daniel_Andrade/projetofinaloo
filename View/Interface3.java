package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;

public class Interface3 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface3 frame = new Interface3();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Interface3() {
		setTitle("Projeto Final OO");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblReceptor = new JLabel("Receptor");
		lblReceptor.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblReceptor.setBounds(254, 0, 85, 43);
		contentPane.add(lblReceptor);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.setBounds(45, 102, 89, 23);
		contentPane.add(btnCadastrar);
		
		JButton btnListarCabelos = new JButton("Listar Cabelos");
		btnListarCabelos.setBounds(45, 136, 118, 23);
		contentPane.add(btnListarCabelos);
		
		JButton btnBuscar = new JButton("Buscar resultados");
		btnBuscar.setBounds(45, 170, 146, 23);
		contentPane.add(btnBuscar);
		
		JButton btnRemover = new JButton("Remover");
		btnRemover.setBounds(45, 204, 89, 23);
		contentPane.add(btnRemover);
		
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.setBounds(45, 238, 89, 23);
		contentPane.add(btnVoltar);
	}

}
