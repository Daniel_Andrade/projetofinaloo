package View;

import java.util.Scanner;
import Controller.ControladorDoador;
import Controller.ControladorReceptor;
import Model.Doador;
import Model.Receptor;

public class Tela {

	public static void main(String[] args) {
		
		int opcao = -1;
		int opcao1 = -1;
		int opcao2 = -1;
		
		Scanner leitor = new Scanner(System.in);
		ControladorDoador umControladorDoador = new ControladorDoador();
		ControladorReceptor umControladorReceptor = new ControladorReceptor();
		Doador umDoador = null;
		Receptor umReceptor = null;
		
		while (opcao != 0){
			System.out.println("--- Software de Doa��o de cabelos ---\n"
					+ "1 - Doador\n"
					+ "2 - Receptor \n"
					+ "0 - Fechar programa\n");
			opcao = leitor.nextInt();
			
			switch(opcao){
			case 1:
				while(opcao1 != 0){
					System.out.println("--- Doador ---\n"
						+ "1 - Cadastrar Doador\n"
						+ "2 - Buscar Doador\n"
						+ "3 - Remover Doador\n"
						+ "0 - Ir para menu de receptor");
					opcao1 = leitor.nextInt();
					
					switch(opcao1){
						case 1:
							System.out.println("Digite o nome: ");
							String nome = leitor.next();
							umDoador = new Doador(nome);
							System.out.println("Digite o telefone: ");
							umDoador.setTelefone(leitor.next());
							System.out.println("Digite seu Email: ");
							umDoador.setEmail(leitor.next());
							System.out.println("Digite seu endere�o: ");
							umDoador.setEndereco(leitor.next());
							System.out.println("Digite sua idade: ");
							umDoador.setIdade(leitor.next());
							System.out.println("Digite a cor do seu cabelo: ");
							umDoador.setCorCabelo(leitor.next());
							System.out.println("Digite o tipo do seu cabelo: ");
							umDoador.setTipoCabelo(leitor.next());
							System.out.println("Digite o tamanho do seu cabelo (curto ou longo): ");
							umDoador.setTamanhoCabelo(leitor.next());
							umControladorDoador.adicionarDoador(umDoador);
							break;
						case 2:
							umControladorDoador.mostraDoador();
							System.out.println("Digite o nome do doador: ");
							umDoador = umControladorDoador.buscarDoador(leitor.next());
							System.out.println("Telefone: "+umDoador.getTelefone());
							System.out.println("Email: "+umDoador.getEmail());
							System.out.println("Endere�o: "+umDoador.getEndereco());
							System.out.println("Idade: "+umDoador.getIdade());
							System.out.println("Cor do cabelo: "+umDoador.getCorCabelo());
							System.out.println("Tipo do cabelo: "+umDoador.getTipoCabelo());
							System.out.println("Tamanho do cabelo: "+umDoador.getTamanhoCabelo());
							break;
						case 3:
							umControladorDoador.mostraDoador();
							System.out.println("Digite o nome do Doador que deeja remover: \n");
							umDoador = umControladorDoador.buscarDoador(leitor.next());
							umControladorDoador.removerDoador(umDoador);
							break;
					}			
				}
			case 2:
				while(opcao2 != 0){
					System.out.println("--- Receptor ---\n"
						+ "1 - Cadastrar Receptor\n"
						+ "2 - Listar cabelos\n"
						+ "3 - Buscar Resultados\n"
						+ "4 - Remover Receptor\n"
						+ "0 - Voltar");
					opcao2 = leitor.nextInt();
					switch(opcao2){
						case 1:
							System.out.println("Digite o nome: ");
							String nome = leitor.next();
							umReceptor = new Receptor(nome);
							System.out.println("Digite o telefone: ");
							umReceptor.setTelefone(leitor.next());
							System.out.println("Digite seu Email: ");
							umReceptor.setEmail(leitor.next());
							System.out.println("Digite seu endere�o: ");
							umReceptor.setEndereco(leitor.next());
							System.out.println("Digite sua idade: ");
							umReceptor.setIdade(leitor.nextInt());
							System.out.println("Digite a cor do cabelo desejado: ");
							umReceptor.setCorCabelo(leitor.next());
							System.out.println("Digite o tipo do cabelo desejado: ");
							umReceptor.setTipoCabelo(leitor.next());
							System.out.println("Digite o tamanho do cabelo desejado(curto ou longo): ");
							umReceptor.setTamanhoCabelo(leitor.next());
							System.out.println("Voce tem prioridade? \ntrue \nfalse");
							umReceptor.setPrioridade(leitor.nextBoolean());
							umControladorReceptor.adicionarReceptor(umReceptor);
							break;
						case 2:
							umControladorDoador.mostraCabeloDoador();					
							break;
						case 3:
							umControladorReceptor.mostraReceptor();
							System.out.println("Digite o nome do receptor: ");
							umReceptor = umControladorReceptor.buscarReceptor(leitor.next());
							System.out.println("Telefone: "+umReceptor.getTelefone());
							System.out.println("Email: "+umReceptor.getEmail());
							System.out.println("Endere�o: "+umReceptor.getEndereco());
							System.out.println("Idade: "+umReceptor.getIdade());
							
							if(umReceptor.Idade <= 12){
								umControladorDoador.efetivar(umReceptor.getCorCabelo(), umReceptor);
								System.out.println("Conseguiu um cabelo?");
								System.out.println("Sim!!");
								System.out.println("Nome do doador: "+ umDoador.getNome()+"\nTelefone: "+umDoador.getTelefone());
							} else{
								System.out.println("Ainda nao.");
							}
							break;
						case 4:
							umControladorReceptor.mostraReceptor();
							System.out.println("Digite o nome do receptor que deeja remover: \n");
							umReceptor = umControladorReceptor.buscarReceptor(leitor.next());
							umControladorReceptor.removerReceptor(umReceptor);
							break;
						default:
							System.out.println("Op��o invalida.");
							break;
					}
				}
			}	
		}
		leitor.close();
	}
}
