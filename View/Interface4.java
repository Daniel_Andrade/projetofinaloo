package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;

public class Interface4 extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface4 frame = new Interface4();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Interface4() {
		setTitle("Projeto Final OO");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDoador = new JLabel("Cadastrar Doador");
		lblDoador.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblDoador.setBounds(208, 0, 178, 55);
		contentPane.add(lblDoador);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(34, 78, 46, 14);
		contentPane.add(lblNome);
		
		textField = new JTextField();
		textField.setBounds(106, 75, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblTelefone = new JLabel("Telefone:");
		lblTelefone.setBounds(34, 109, 46, 14);
		contentPane.add(lblTelefone);
		
		textField_1 = new JTextField();
		textField_1.setBounds(106, 106, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(106, 137, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(106, 168, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Email:");
		lblNewLabel.setBounds(34, 140, 46, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblEndereo = new JLabel("Endere\u00E7o:");
		lblEndereo.setBounds(38, 171, 58, 14);
		contentPane.add(lblEndereo);
		
		textField_4 = new JTextField();
		textField_4.setBounds(106, 199, 86, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblIdade = new JLabel("Idade:");
		lblIdade.setBounds(34, 202, 46, 14);
		contentPane.add(lblIdade);
		
		textField_5 = new JTextField();
		textField_5.setBounds(106, 230, 86, 20);
		contentPane.add(textField_5);
		textField_5.setColumns(10);
		
		JLabel lblCorDoCabelo = new JLabel("Cor do cabelo:");
		lblCorDoCabelo.setBounds(26, 233, 70, 14);
		contentPane.add(lblCorDoCabelo);
		
		textField_6 = new JTextField();
		textField_6.setBounds(106, 261, 86, 20);
		contentPane.add(textField_6);
		textField_6.setColumns(10);
		
		JLabel lblTipoDoCabelo = new JLabel("Tipo do cabelo:");
		lblTipoDoCabelo.setBounds(24, 264, 86, 14);
		contentPane.add(lblTipoDoCabelo);
		
		textField_7 = new JTextField();
		textField_7.setBounds(106, 292, 86, 20);
		contentPane.add(textField_7);
		textField_7.setColumns(10);
		
		JLabel lblTamanhoDoCabelo = new JLabel("Tamanho do cabelo:");
		lblTamanhoDoCabelo.setBounds(10, 295, 110, 14);
		contentPane.add(lblTamanhoDoCabelo);
		
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.setBounds(429, 227, 89, 23);
		contentPane.add(btnVoltar);
		
		JButton btnAvanar = new JButton("Avan\u00E7ar");
		btnAvanar.setBounds(429, 136, 89, 23);
		contentPane.add(btnAvanar);
	}
}
