package Model;

public class Doador{
	
	String nome;
	String Telefone;
	String Email;
	String Endereco;
	String Idade;

	String corCabelo;
	String tipoCabelo;
	String tamanhoCabelo;
	
	
	public Doador(String nome){
		this.nome = nome;
	}
	
	public Doador(String nome, String corCabelo){
		this.corCabelo = corCabelo;
	}
	
	
	public String getCorCabelo() {
		return corCabelo;
	}

	public void setCorCabelo(String corCabelo) {
		this.corCabelo = corCabelo;
	}

	public String getTipoCabelo() {
		return tipoCabelo;
	}

	public void setTipoCabelo(String tipoCabelo) {
		this.tipoCabelo = tipoCabelo;
	}

	public String getTamanhoCabelo() {
		return tamanhoCabelo;
	}

	public void setTamanhoCabelo(String tamanhoCabelo) {
		this.tamanhoCabelo = tamanhoCabelo;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTelefone() {
		return Telefone;
	}
	public void setTelefone(String telefone) {
		Telefone = telefone;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getEndereco() {
		return Endereco;
	}
	public void setEndereco(String endereco) {
		Endereco = endereco;
	}
	public String getIdade() {
		return Idade;
	}
	public void setIdade(String idade) {
		Idade = idade;
	}
	
}
