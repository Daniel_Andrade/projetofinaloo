package Model;

public class Receptor{
	
	String nome;
	String Telefone;
	String Email;
	String Endereco;
	public int Idade;
	Boolean Prioridade;
	
	String corCabelo;
	String tipoCabelo;
	String tamanhoCabelo;
	
	
	public Receptor(String nome){
		this.nome = nome;
	}
		
	public String getCorCabelo() {
		return corCabelo;
	}


	public void setCorCabelo(String corCabelo) {
		this.corCabelo = corCabelo;
	}


	public String getTipoCabelo() {
		return tipoCabelo;
	}


	public void setTipoCabelo(String tipoCabelo) {
		this.tipoCabelo = tipoCabelo;
	}


	public String getTamanhoCabelo() {
		return tamanhoCabelo;
	}


	public void setTamanhoCabelo(String tamanhoCabelo) {
		this.tamanhoCabelo = tamanhoCabelo;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTelefone() {
		return Telefone;
	}
	public void setTelefone(String telefone) {
		Telefone = telefone;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getEndereco() {
		return Endereco;
	}
	public void setEndereco(String endereco) {
		Endereco = endereco;
	}
	public int getIdade() {
		return Idade;
	}
	public void setIdade(int idade) {
		Idade = idade;
	}
	public Boolean getPrioridade() {
		return Prioridade;
	}
	public void setPrioridade(Boolean prioridade) {
		Prioridade = prioridade;
	}
}
