package Controller;
import java.util.ArrayList;
import Model.Receptor;

public class ControladorReceptor {
	
	ArrayList<Receptor> listaReceptores;
	
	public ControladorReceptor(){
		listaReceptores = new ArrayList<Receptor>();
	}
	
	public void adicionarReceptor(Receptor umReceptor){
		listaReceptores.add(umReceptor);
	}
	
	public void removerReceptor(Receptor umReceptor){
		listaReceptores.remove(umReceptor);
	}
	
	public Receptor buscarReceptor(String umNome){
		for (Receptor umReceptor: listaReceptores){
			if (umReceptor.getNome().equalsIgnoreCase(umNome)){
				return umReceptor;
			}
		}
		
		return null;
	}
	
	public void mostraReceptor(){
		for (Receptor umReceptor: listaReceptores){
			System.out.println(umReceptor.getNome());
		}
	}
	
	

}
