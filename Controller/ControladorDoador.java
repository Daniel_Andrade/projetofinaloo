package Controller;
import java.util.ArrayList;
import Model.Doador;
import Model.Receptor;

public class ControladorDoador {
	
	ArrayList<Doador> listaDoadores;
	
	public ControladorDoador(){
		listaDoadores = new ArrayList<Doador>();
	}
	
	public void adicionarDoador(Doador umDoador){
		listaDoadores.add(umDoador);
	}
	
	public void removerDoador(Doador umDoador){
		listaDoadores.remove(umDoador);
	}
	
	public Doador buscarDoador(String umNome){
		for (Doador umDoador: listaDoadores){
			if (umDoador.getNome().equalsIgnoreCase(umNome)){
				return umDoador;
			}
		}
		
		return null;
	}
	
	public Doador buscarCabeloDoador(String umaCor){
		for (Doador umDoador: listaDoadores){
			if (umDoador.getCorCabelo().equalsIgnoreCase(umaCor)){
				return umDoador;
			}
		}
		
		return null;
	}
	
	public void mostraDoador(){
		for (Doador umDoador: listaDoadores){
			System.out.println(umDoador.getNome());
		}
	}

	public void mostraCabeloDoador(){
		for (Doador umDoador: listaDoadores){
			System.out.println(umDoador.getCorCabelo());
		}
	}
	
	public void efetivar(String umaCor, Receptor umReceptor){
		for (Doador umDoador: listaDoadores){
			if (umDoador.getCorCabelo().equalsIgnoreCase(umaCor)){
				umReceptor.setCorCabelo(umDoador.getCorCabelo());
				umReceptor.setTamanhoCabelo(umDoador.getTamanhoCabelo());
				umReceptor.setTipoCabelo(umDoador.getTipoCabelo());
			}
		}
	}
}
	
