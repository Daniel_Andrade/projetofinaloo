package Controller;
import java.util.ArrayList;

import Model.Cabelo;
import Model.Receptor;

public class ControladorCabelo {
	
	Receptor umReceptor;
	ArrayList<Cabelo> listaCabelos;
	
	public ControladorCabelo(){
		listaCabelos = new ArrayList<Cabelo>();
	}
	
	public void adicionarCabelo(Cabelo umCabelo){
		listaCabelos.add(umCabelo);
	}
	
	public void removerCabelo(Cabelo umCabelo){
		listaCabelos.remove(umCabelo);
	}
	
	public void listarCabelo(Cabelo umCabelo){
		System.out.println("Cor: " + umCabelo.getCor());
		System.out.println("Tipo: " + umCabelo.getTipo());
		System.out.println("Tamanho: " + umCabelo.getTamanho());		
	}
}
